/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.gui;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;
import tluszczcalc.engine.AbstractAlgorithm;
import tluszczcalc.types.BaseEnum;

/**
 *
 * @author clutroth
 */
public class MouseManager implements MouseListener {

    MouseManager(Map<Enum, InputPanel> formInputs) {
        this.formInputs = formInputs;
        this.defaultBorder = formInputs.values().iterator().next().getBorder();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        AlgorithmInputPanel aip = (AlgorithmInputPanel) me.getSource();
        for (Object o : aip.getParameterSet()) {
            InputPanel ip = formInputs.get(o);
            ip.setBorder(BorderFactory.createLineBorder(Color.red));
        }
    }

    @Override
    public void mouseExited(MouseEvent me) {
        AlgorithmInputPanel aip = (AlgorithmInputPanel) me.getSource();
        for (Object o : aip.getParameterSet()) {
            InputPanel ip = formInputs.get(o);
            ip.setBorder(defaultBorder);
        }
    }

    private Border defaultBorder;
    private Map<Enum, InputPanel> formInputs;
}
