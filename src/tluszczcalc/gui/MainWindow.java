/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import tluszczcalc.engine.AbstractAlgorithm;
import tluszczcalc.engine.FemaleJacksonPollock4;
import tluszczcalc.engine.MaleJacksonPollock4;
import tluszczcalc.types.MaleAlgorithmsEnum;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.FemaleAlgorithmsEnum;
import tluszczcalc.types.FemaleCaliperMeasurements;
import tluszczcalc.types.FemaleTapeMeasurements;
import tluszczcalc.types.MaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;
import tluszczcalc.types.MaleTapeMeasurements;

/**
 *
 * @author clutroth
 */
public class MainWindow extends javax.swing.JFrame {

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();

        forms = new ArrayList<FormPanel>();
        formInputs = new HashMap<Enum, InputPanel>();
        algorithmInputs = new HashMap<Enum, InputPanel>();
        OKBtn = new JButton(OK_TEXT);
        OKBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                Class<AbstractAlgorithm> alg;
                Method calc;
                for (Enum en : algorithmInputs.keySet()) {
                    try {
                        BaseEnum be = (BaseEnum) en;
                        AbstractAlgorithm aa = (AbstractAlgorithm) be.getType().newInstance();
                        Double result;
                        if (getParamMap().keySet().containsAll(aa.getParams())) {
                            calc = be.getType().getMethod("calculate", Map.class);
                            Object o = calc.invoke(aa, getParamMap());
                            result = (Double) o;
                        } else {
                            result = new Double(0);
                        }
                        algorithmInputs.get(en).getTextField().setText(String.format("%2.2f", result));
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchMethodException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SecurityException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

//            public void actionPerformed(ActionEvent e) {
//                
        setLocation(0, 0);
        ButtonGroup sexGroup = new ButtonGroup();
        controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        controlPanel.add(OKBtn);
        ActionListener sexRadioBtnListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                JRadioButton btn = (JRadioButton) ae.getSource();
                if (btn.getText().equals(MALE_TXT)) {
                    initForms(MaleTapeMeasurements.values(), MaleAlgorithmsEnum.values(), MaleCaliperMeasurements.values());
                } else if (btn.getText().equals(FEMALE_TXT)) {
                    initForms(FemaleTapeMeasurements.values(), FemaleAlgorithmsEnum.values(), FemaleCaliperMeasurements.values());
                } else {
                    throw new IllegalStateException();
                }
            }
        };
        JRadioButton maleBtn = new JRadioButton(MALE_TXT, true);
        maleBtn.addActionListener(sexRadioBtnListener);
        sexGroup.add(maleBtn);
        controlPanel.add(maleBtn);
        JRadioButton femaleBtn = new JRadioButton(FEMALE_TXT);
        femaleBtn.addActionListener(sexRadioBtnListener);
        sexGroup.add(femaleBtn);
        controlPanel.add(femaleBtn);
        initForms(MaleTapeMeasurements.values(), MaleAlgorithmsEnum.values(), MaleCaliperMeasurements.values());

    }

    private Map<Enum, Object> getParamMap() {
        Map<Enum, Object> m = new HashMap<Enum, Object>();
        for (Enum e : formInputs.keySet()) {
            String value = formInputs.get(e).getValue();
            if (value != null && value.equals("") == false && Double.parseDouble(value) >= 0) {
                m.put(e, value);
            }
        }
        return m;
    }

    private void initForms(Enum[] tape, Enum[] algorithm, Enum[] caliper) {
        try {
            forms.clear();
            formInputs.clear();
            algorithmInputs.clear();
            getContentPane().removeAll();
            getContentPane().setLayout(new GridBagLayout());
            FormPanel fp;
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 0;
            c.gridy = 0;
            fp = new FormPanel(tape, "Pomiary Taśmą");
            formInputs.putAll(fp.getFields());
            getContentPane().add(fp, c);

            c.gridx = 0;
            c.gridy = 1;
            fp = new FormPanel(OtherMeasurements.values(), "Inne wielkości");
            formInputs.putAll(fp.getFields());
            getContentPane().add(fp, c);

            c.gridx = 1;
            c.gridy = 0;
            c.gridheight = 2;
            fp = new FormPanel(caliper, CALIPER_MEASUREMENTS);
            formInputs.putAll(fp.getFields());
            getContentPane().add(fp, c);

            c.gridx = 2;
            c.gridy = 0;
            c.gridheight = 1;
            fp = new FormPanel(algorithm, "Metody liczenia");
            algorithmInputs.putAll(fp.getFields());
            getContentPane().add(fp, c);

            c.gridx = 2;
            c.gridy = 1;
            c.gridheight = 1;
            getContentPane().add(controlPanel, c);

        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        mm = new MouseManager(formInputs);
        for (InputPanel ip : algorithmInputs.values()) {
            ip.addMouseListener(mm);
        }
        pack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }
    private List<FormPanel> forms;
    private Map<Enum, InputPanel> formInputs;
    private Map<Enum, InputPanel> algorithmInputs;
    private final JButton OKBtn;
    private MouseManager mm;
    private JPanel controlPanel;

    private static final String OK_TEXT = "Licz!";
    private static final String CALIPER_MEASUREMENTS = "Pomiary caliperem";
    private static final String MALE_TXT = "Meżczyzna";
    private static final String FEMALE_TXT = "Kobieta";

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
