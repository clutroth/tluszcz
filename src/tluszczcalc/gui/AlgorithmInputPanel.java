/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.gui;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import tluszczcalc.engine.AbstractAlgorithm;
import tluszczcalc.types.BaseEnum;

/**
 *
 * @author clutroth
 */
public class AlgorithmInputPanel extends InputPanel {

    public AlgorithmInputPanel(Enum en) {
        super(en);
        try {
            BaseEnum be = (BaseEnum) en;
            AbstractAlgorithm algorithmObject = (AbstractAlgorithm) be.getType().newInstance();
            parameterSet = algorithmObject.getParams();
        } catch (InstantiationException ex) {
            Logger.getLogger(AlgorithmInputPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AlgorithmInputPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private Set parameterSet;

    /**
     * @return the parameterSet
     */
    public Set getParameterSet() {
        return parameterSet;
    }
}
