/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

/**
 *
 * @author clutroth
 */
public enum MaleCaliperMeasurements implements BaseEnum {

    ABDOMINAL("brzuch"),
    BICEP("biceps"),
    CALF("łydka"),
    CHEST("klatka"),
    LOW_BACK("plecy"),
    MIDAXILLARY("pacha"),
    SUBSCAPULAR("łopatka"),
    SUPRAILIAC("biodro"),
    TIGH("udo"),
    TRICEP("triceps");

    private MaleCaliperMeasurements(String name) {
        this.name = name;
    }

    private String name;
    private static Class type = int.class;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class getType() {
        return type;
    }
};
