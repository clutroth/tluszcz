/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

/**
 *
 * @author clutroth
 */
public enum OtherMeasurements implements BaseEnum {

    WEIGHT("waga", double.class),
    AGE("wiek", int.class);
    private String name;
    private Class type;

    private OtherMeasurements(String name, Class type) {
        this.name = name;
        this.type = type;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class getType() {
        return type;
    }
}
