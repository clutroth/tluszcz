/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

import java.io.Serializable;

/**
 *
 * @author clutroth
 */
public interface BaseEnum {

    /**
     * @return the name
     */
    public String getName();

    /**
     * @return the type
     */
    public Class getType();

    @Override
    public String toString();
}
