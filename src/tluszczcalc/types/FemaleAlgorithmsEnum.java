/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

import tluszczcalc.engine.AbstractAlgorithm;
import tluszczcalc.engine.FemaleDurninWomersley;
import tluszczcalc.engine.FemaleJacksonPollock3;
import tluszczcalc.engine.FemaleJacksonPollock4;
import tluszczcalc.engine.FemaleJacksonPollock7;
import tluszczcalc.engine.FemaleParillo;
import tluszczcalc.engine.FemaleUSNavy;

/**
 *
 * @author clutroth
 */
public enum FemaleAlgorithmsEnum implements BaseEnum {

    DURNIN_WOMERSLEY("Durnina=Wormsley'a", FemaleDurninWomersley.class),
    US_NAVY("Armii Amerykańskiej", FemaleUSNavy.class),
    PARIOLLO("Pariollo", FemaleParillo.class),
    JACKSON_POLLOCK3("Jackson'a-Pollocka'a 3 pomiary", FemaleJacksonPollock3.class),
    JACKSON_POLLOCK4("Jackson'a-Pollocka'a 4 pomiary", FemaleJacksonPollock4.class),
    JACKSON_POLLOCK7("Jackson'a-Pollocka'a 7 pomiarów", FemaleJacksonPollock7.class);

    private FemaleAlgorithmsEnum(String name, Class<? extends AbstractAlgorithm> algorithm) {
        this.name = name;
        this.type = algorithm;
    }

    private String name;
    private Class<? extends AbstractAlgorithm> type;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class getType() {
        return type;
    }
}
