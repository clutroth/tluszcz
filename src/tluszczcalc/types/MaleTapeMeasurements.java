/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

/**
 *
 * @author clutroth
 */
public enum MaleTapeMeasurements implements BaseEnum {

    HEIGHT("wzrost"),
    WAIST("talia"),
    NECK("szyja");

    private MaleTapeMeasurements(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public Class getType() {
        return type;
    }
    private String name;
    private static final Class type = double.class;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
}
