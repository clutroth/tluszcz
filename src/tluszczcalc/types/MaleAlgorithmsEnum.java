/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.types;

import tluszczcalc.engine.AbstractAlgorithm;
import tluszczcalc.engine.MaleDurninWomersley;
import tluszczcalc.engine.MaleJacksonPollock3;
import tluszczcalc.engine.MaleJacksonPollock4;
import tluszczcalc.engine.MaleJacksonPollock7;
import tluszczcalc.engine.MaleParillo;
import tluszczcalc.engine.MaleUSNavy;

/**
 *
 * @author clutroth
 */
public enum MaleAlgorithmsEnum implements BaseEnum {

    DURNIN_WOMERSLEY("Durnina=Wormsley'a", MaleDurninWomersley.class),
    US_NAVY("Armii Amerykańskiej", MaleUSNavy.class),
    PARIOLLO("Pariollo", MaleParillo.class),
    JACKSON_POLLOCK3("Jackson'a-Pollocka'a 3 pomiary", MaleJacksonPollock3.class),
    JACKSON_POLLOCK4("Jackson'a-Pollocka'a 4 pomiary", MaleJacksonPollock4.class),
    JACKSON_POLLOCK7("Jackson'a-Pollocka'a 7 pomiarów", MaleJacksonPollock7.class);

    private MaleAlgorithmsEnum(String name, Class<? extends AbstractAlgorithm> algorithm) {
        this.name = name;
        this.type = algorithm;
    }

    private String name;
    private Class<? extends AbstractAlgorithm> type;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class getType() {
        return type;
    }
}
