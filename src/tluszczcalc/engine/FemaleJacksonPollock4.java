/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.FemaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class FemaleJacksonPollock4 extends AbstractAlgorithm {

    @Override
    protected double[][] getFactoryTable() {
        double[][] f = {{0.29669, -0.00043, 0.02963, 1.4072}};
        return f;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(FemaleCaliperMeasurements.ABDOMINAL);
        s.add(FemaleCaliperMeasurements.TRICEP);
        s.add(FemaleCaliperMeasurements.TIGH);
        s.add(FemaleCaliperMeasurements.SUPRAILIAC);
        s.add(OtherMeasurements.AGE);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double sum = 0;
        Enum[] pm = {
            FemaleCaliperMeasurements.ABDOMINAL,
            FemaleCaliperMeasurements.TRICEP,
            FemaleCaliperMeasurements.TIGH,
            FemaleCaliperMeasurements.SUPRAILIAC,};
        for (Enum p : pm) {
            sum += Double.parseDouble((String) param.get(p));
        }
        int age = (int) Double.parseDouble((String) param.get(OtherMeasurements.AGE));
        return (factories[0] * sum) + (factories[1] * sum * sum) + (factories[2] * age) + factories[3];
    }

}
