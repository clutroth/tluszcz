package tluszczcalc.engine;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.OtherMeasurements;

public abstract class AbstractAlgorithm {

    protected int[] getAgeArray() {
        int[] a = {Integer.MAX_VALUE};
        return a;
    }

    protected abstract double[][] getFactoryTable();

    public abstract Set<BaseEnum> getParams();

    protected double equation(double bd) {
        return bd;
    }

    public final Double calculate(Map<Enum, Object> param) {
        double[] factories = getFactories(param);
        return equation(formula(factories, param));
    }

    public boolean hasParams(Map<Enum, Object> paramMap) {
        for (Object o : getParams()) {
            try {
                Double.parseDouble((String) paramMap.get(o));
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    protected abstract double formula(double[] factories, Map<Enum, Object> param);

    private double[] getFactories(Map<Enum, Object> param) {
        int age = Integer.parseInt((String) param.get(OtherMeasurements.AGE));
        int[] ageArray = getAgeArray();
        Arrays.sort(ageArray);
        int i;
        for (i = 0; i < ageArray.length && ageArray[i] <= age; i++)
            ;
        return getFactoryTable()[i];

    }
}
