/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.MaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class MaleJacksonPollock7 extends AbstractAlgorithm {

    @Override
    protected double equation(double bd) {
        return Equations.siri(bd);
    }

    @Override
    protected double[][] getFactoryTable() {
        double[][] f = {{-0.00043499, +0.00000055, -0.00028826, 1.112}};
        return f;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(MaleCaliperMeasurements.ABDOMINAL);
        s.add(MaleCaliperMeasurements.MIDAXILLARY);
        s.add(MaleCaliperMeasurements.CHEST);
        s.add(MaleCaliperMeasurements.SUBSCAPULAR);
        s.add(MaleCaliperMeasurements.SUPRAILIAC);
        s.add(MaleCaliperMeasurements.TRICEP);
        s.add(MaleCaliperMeasurements.TIGH);
        s.add(OtherMeasurements.AGE);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double sum = 0;
        Enum[] pm = {
            MaleCaliperMeasurements.ABDOMINAL,
            MaleCaliperMeasurements.CHEST,
            MaleCaliperMeasurements.MIDAXILLARY,
            MaleCaliperMeasurements.SUBSCAPULAR,
            MaleCaliperMeasurements.SUPRAILIAC,
            MaleCaliperMeasurements.TRICEP,
            MaleCaliperMeasurements.TIGH,};
        for (Enum p : pm) {
            sum += Double.parseDouble((String) param.get(p));
        }
        int age = (int) Double.parseDouble((String) param.get(OtherMeasurements.AGE));
        return (factories[0] * sum) + (factories[1] * sum * sum) + (factories[2] * age) + factories[3];
    }

}
