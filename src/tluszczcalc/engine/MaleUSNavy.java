/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.MaleTapeMeasurements;

/**
 *
 * @author clutroth
 */
public class MaleUSNavy extends AbstractAlgorithm {

    @Override
    protected double[][] getFactoryTable() {
        double[][] resp = {{86.010, 70.041, 36.76}};
        return resp;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(MaleTapeMeasurements.WAIST);
        s.add(MaleTapeMeasurements.NECK);
        s.add(MaleTapeMeasurements.HEIGHT);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double neck = Double.parseDouble((String) param.get(MaleTapeMeasurements.NECK));
        double waist = Double.parseDouble((String) param.get(MaleTapeMeasurements.WAIST));
        double height = Double.parseDouble((String) param.get(MaleTapeMeasurements.HEIGHT));
        return factories[0] * Math.log10(waist - neck) - factories[1] * Math.log10(height) + factories[2];
    }

}
