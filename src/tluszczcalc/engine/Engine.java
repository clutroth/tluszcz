package tluszczcalc.engine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Engine {

    public static final String[] CALIPER_FIELDS = {"abdominal", "bicep", "calf",
        "chest", "lowBack", "midaxillary", "subscapular", "suprailac", "tigh",
        "tricep"};
    public static final String[] TAPE_FIELDS = {"height", "hips", "neck", "waist"};
    public static final String[] OTHER_FIELDS = {"weight", "age"};

    public Engine() {

    }

}
