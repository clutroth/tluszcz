/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.FemaleTapeMeasurements;

/**
 *
 * @author clutroth
 */
public class FemaleUSNavy extends AbstractAlgorithm {

    @Override
    protected double[][] getFactoryTable() {
        double[][] resp = {{163.205, 97.684, 104.912}};
        return resp;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(FemaleTapeMeasurements.WAIST);
        s.add(FemaleTapeMeasurements.NECK);
        s.add(FemaleTapeMeasurements.HIPS);
        s.add(FemaleTapeMeasurements.HEIGHT);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double hips = Double.parseDouble((String) param.get(FemaleTapeMeasurements.HIPS));
        double neck = Double.parseDouble((String) param.get(FemaleTapeMeasurements.NECK));
        double waist = Double.parseDouble((String) param.get(FemaleTapeMeasurements.WAIST));
        double height = Double.parseDouble((String) param.get(FemaleTapeMeasurements.HEIGHT));
        return factories[0] * Math.log10(waist - neck + hips) - factories[1] * Math.log10(height) + factories[2];
    }

}
