package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.FemaleCaliperMeasurements;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class FemaleDurninWomersley extends AbstractAlgorithm {

    /**
     *
     */
    public FemaleDurninWomersley() {
        super();
    }

    /**
     *
     * @param bd Body Density - gestosc ciala
     * @return Zawartosc procentowa tluszczu w organizmie
     */
    @Override
    protected double equation(double bd) {
        return Equations.siri(bd);
    }

    /**
     *
     * @return Zwraca zbior parametrow, ktore wykozystuje algorytm, do
     * wyliczenia zawartosci procentowej tluszczu w organizmie
     */
    @Override
    public Set<BaseEnum> getParams() {
        Set<BaseEnum> s = new HashSet();
        s.add(FemaleCaliperMeasurements.BICEP);
        s.add(FemaleCaliperMeasurements.TRICEP);
        s.add(FemaleCaliperMeasurements.SUBSCAPULAR);
        s.add(FemaleCaliperMeasurements.SUPRAILIAC);
        s.add(OtherMeasurements.AGE);

        return s;
    }

    /**
     * W tej metodzie znajduje sie cala logika lagorytmu
     *
     * @param arg tablica wspolczynnikow wykorzystywanych podczas obliczania
     * zawartosci procentowej tluszczu. Parametry maja wartosc odpowiednia do
     * wieku i plci badanego.
     * @param param mapa prarametrow i wartosci potrzebnych do wyznaczenia ZPT
     * @return ZPT lub gestosc ciala zaleznie od wzoru.
     */
    @Override
    protected double formula(double[] arg, Map<Enum, Object> param) {
        double sum = 0;
        Enum[] measurments = {
            FemaleCaliperMeasurements.BICEP,
            FemaleCaliperMeasurements.TRICEP,
            FemaleCaliperMeasurements.SUBSCAPULAR,
            FemaleCaliperMeasurements.SUPRAILIAC
        };
        for (Enum key : measurments) {
            String v = (String) param.get(key);
            sum += (double) Double.parseDouble(v);
        }
        return arg[0] - arg[1] * Math.log10(sum);
    }

    /**
     *
     * @return tablica lat, tworzaca przedzialy dla ktorych parametry we wzorze
     * sa stale
     */
    @Override
    protected int[] getAgeArray() {
        /*
         Ta tablica definiuje przedzialy:
         do 17, 17-19, 20-29, 30-39, 40-49, powyzej 50
         */
        int[] ageArray = {17, 20, 30, 40, 50};
        return ageArray;
    }

    /**
     *
     * @return Zwraca macierz wspolczynnikow. wiersze odpowiadaja przedzialom
     * wiekowym. Liczba kolumn jest zalezna od ilosci wspolczynnikow
     * wykorzystywanych przez algorytm.
     */
    @Override
    protected double[][] getFactoryTable() {
        double[][] factoryTable = {
            // Females_____________Age
            {1.1369, 0.0589}, // < 17
            {1.1549, 0.0678}, // <17;19)
            {1.1599, 0.0717}, // <20;29)
            {1.1423, 0.0632}, // <30;39)
            {1.1333, 0.0612}, // <40;49)
            {1.1339, 0.0645} // > 50
        };
        return factoryTable;
    }

}
