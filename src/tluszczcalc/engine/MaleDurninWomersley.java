package tluszczcalc.engine;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.MaleCaliperMeasurements;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.OtherMeasurements;

public class MaleDurninWomersley extends AbstractAlgorithm {

    public MaleDurninWomersley() {
        super();
    }

    protected double equation(double bd) {
        return Equations.siri(bd);
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set<BaseEnum> s = new HashSet();
        s.add(MaleCaliperMeasurements.BICEP);
        s.add(MaleCaliperMeasurements.TRICEP);
        s.add(MaleCaliperMeasurements.SUBSCAPULAR);
        s.add(MaleCaliperMeasurements.SUPRAILIAC);
        s.add(OtherMeasurements.AGE);

        return s;
    }

    @Override
    protected double formula(double[] arg, Map<Enum, Object> param) {
        double sum = 0;
        Enum[] measurments = {
            MaleCaliperMeasurements.BICEP,
            MaleCaliperMeasurements.TRICEP,
            MaleCaliperMeasurements.SUBSCAPULAR,
            MaleCaliperMeasurements.SUPRAILIAC
        };
        for (Enum key : measurments) {
            String v = (String) param.get(key);
            sum += (double) Double.parseDouble(v);
        }
        return arg[0] - arg[1] * Math.log10(sum);
    }

    @Override
    protected int[] getAgeArray() {
        int[] ageArray = {17, 20, 30, 40, 50};
        return ageArray;
    }

    @Override
    protected double[][] getFactoryTable() {
        double[][] factoryTable = {
            // Males_____________Age
            {1.1533, 0.0643}, // < 17
            {1.1620, 0.0630}, // <17;19)
            {1.1631, 0.0632}, // <20;29)
            {1.1422, 0.0544}, // <30;39)
            {1.1620, 0.0700}, // <40;49)
            {1.1715, 0.0779} // > 50
        };
        return factoryTable;
    }

}
