/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.FemaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class FemaleJacksonPollock3 extends AbstractAlgorithm {

    @Override
    protected double equation(double bd) {
        return Equations.siri(bd);
    }

    @Override
    protected double[][] getFactoryTable() {
        double[][] f = {{-0.0009929, +0.0000023, -0.0001392, 1.0994921}};
        return f;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(FemaleCaliperMeasurements.TIGH);
        s.add(FemaleCaliperMeasurements.TRICEP);
        s.add(FemaleCaliperMeasurements.SUPRAILIAC);
        s.add(OtherMeasurements.AGE);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double sum = 0;
        Enum[] pm = {
            FemaleCaliperMeasurements.TIGH,
            FemaleCaliperMeasurements.TRICEP,
            FemaleCaliperMeasurements.SUPRAILIAC,};
        for (Enum p : pm) {
            sum += Double.parseDouble((String) param.get(p));
        }
        int age = (int) Double.parseDouble((String) param.get(OtherMeasurements.AGE));
        return (factories[0] * sum) + (factories[1] * sum * sum) + (factories[2] * age) + factories[3];
    }

}
