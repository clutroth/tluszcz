/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.FemaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class FemaleParillo extends AbstractAlgorithm {

    @Override
    protected double[][] getFactoryTable() {
        double[][] r = {{0}};
        return r;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(FemaleCaliperMeasurements.ABDOMINAL);
        s.add(FemaleCaliperMeasurements.CHEST);
        s.add(FemaleCaliperMeasurements.TIGH);
        s.add(FemaleCaliperMeasurements.BICEP);
        s.add(FemaleCaliperMeasurements.TRICEP);
        s.add(FemaleCaliperMeasurements.SUPRAILIAC);
        s.add(FemaleCaliperMeasurements.SUBSCAPULAR);
        s.add(FemaleCaliperMeasurements.LOW_BACK);
        s.add(FemaleCaliperMeasurements.CALF);
        s.add(OtherMeasurements.WEIGHT);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double sum = 0;
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.ABDOMINAL));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.CHEST));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.TIGH));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.BICEP));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.TRICEP));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.SUPRAILIAC));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.SUBSCAPULAR));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.LOW_BACK));
        sum += Double.parseDouble((String) param.get(FemaleCaliperMeasurements.CALF));
        return sum * 27 / Double.parseDouble((String) param.get(OtherMeasurements.WEIGHT));
    }

}
