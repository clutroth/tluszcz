/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tluszczcalc.engine;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tluszczcalc.types.BaseEnum;
import tluszczcalc.types.MaleCaliperMeasurements;
import tluszczcalc.types.OtherMeasurements;

/**
 *
 * @author clutroth
 */
public class MaleParillo extends AbstractAlgorithm {

    @Override
    protected double[][] getFactoryTable() {
        double[][] r = {{0}};
        return r;
    }

    @Override
    public Set<BaseEnum> getParams() {
        Set s = new HashSet();
        s.add(MaleCaliperMeasurements.ABDOMINAL);
        s.add(MaleCaliperMeasurements.CHEST);
        s.add(MaleCaliperMeasurements.TIGH);
        s.add(MaleCaliperMeasurements.BICEP);
        s.add(MaleCaliperMeasurements.TRICEP);
        s.add(MaleCaliperMeasurements.SUPRAILIAC);
        s.add(MaleCaliperMeasurements.SUBSCAPULAR);
        s.add(MaleCaliperMeasurements.LOW_BACK);
        s.add(MaleCaliperMeasurements.CALF);
        s.add(OtherMeasurements.WEIGHT);
        return s;
    }

    @Override
    protected double formula(double[] factories, Map<Enum, Object> param) {
        double sum = 0;
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.ABDOMINAL));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.CHEST));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.TIGH));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.BICEP));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.TRICEP));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.SUPRAILIAC));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.SUBSCAPULAR));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.LOW_BACK));
        sum += Double.parseDouble((String) param.get(MaleCaliperMeasurements.CALF));
        return sum * 27 / Double.parseDouble((String) param.get(OtherMeasurements.WEIGHT));
    }

}
